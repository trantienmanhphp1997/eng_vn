import React, { useMemo } from "react";
import { Pagination, Space, Table } from "antd";

const TableCustom = ({ data, columns,total,pageIndex,pageSize }: any) => {
  const actionCustom = useMemo(() => {
    let cols =[...columns,
      {
        title: "Action",
        key: "action",
        render: (_, record) => (
          <Space size="middle">
            <a>Invite {record.name}</a>
            <a>Delete</a>
          </Space>
        )
      },
    ]
    return cols
  }, [])
  return <>
  <Table columns={actionCustom} dataSource={data}  />;
  <Pagination defaultCurrent={1} total={total} current={pageIndex} defaultPageSize={10}/>
  </>
};

export default TableCustom;
