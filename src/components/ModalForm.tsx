import { Button, Form, Input, Modal } from "antd";
import React from "react";

const ModalForm = ({ title, open, toggle, submit, children }) => {
  return (
    <Modal title={title} open={open}  footer={null}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={submit}
        autoComplete="off"

      >
        {children}

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" onClick={toggle}>
            Cancel
          </Button>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Ok
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalForm;
