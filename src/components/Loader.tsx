import React from "react";
import { Suspense, lazy } from "react";

const Loader = (Component: any) => (props: any) => {
  const LazyComponent = lazy(Component);
  return (
    <Suspense fallback={<>loading</>}>
      <LazyComponent {...props} />
    </Suspense>
  );
};

export default Loader;
