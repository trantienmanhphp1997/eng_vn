import { Breadcrumb } from "antd";
import React from "react";

const BreadcrumbLayout = ({ children: any }) => {
  return (
    <>
      <Breadcrumb
        className="breadcrumb"
        items={[
          {
            title: "Home",
          },
          {
            title: <a href="">User</a>,
          },
        ]}
      />
    </>
  );
};

export default BreadcrumbLayout;
