import React from "react";
import { Flex, Menu, MenuProps } from "antd";
import { FaUser } from "react-icons/fa";
import { MdOutlineDashboard } from "react-icons/md";
import "../style.css";
// import Logo from '../assets/images/bee.png'

const SidebarLayout = () => {
  type MenuItem = Required<MenuProps>["items"][number];

  const items: MenuItem[] = [
    {
      key: "2",
      label: "Dashboard",
      icon: <MdOutlineDashboard />,
    },
    {
      key: "1",
      label: "User",
      icon: <FaUser />,
    },
  ];
  return (
    <>
      <Flex>
        <div className="logo">{/* <img src={logo} /> */}</div>
      </Flex>
      <Menu
        style={{ height: "100vh" }}
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        items={items}
        theme="dark"
        className="menu-bar"
      />
    </>
  );
};

export default SidebarLayout;
