import { Avatar, Flex, Typography } from "antd";
import React from "react";
import { IoNotificationsOutline } from "react-icons/io5";
import { FaRegUser } from "react-icons/fa6";
import "../style.css";

const HeaderLayout = () => {
  return (
    <>
      <Flex
        style={{
          alignItems: "center",
          justifyContent: "space-between",
          // width: "85vw",
        }}
      >
        <Typography.Text
          type="secondary"
          style={{
            fontSize: "30px",
            color: "black",
            fontWeight: 600,
          }}
        >
          User
        </Typography.Text>
        <Flex
          style={{
            alignItems: "center",
            gap: "1rem",
          }}
        >
          <IoNotificationsOutline className="header-icon" />
          <Avatar icon={<FaRegUser />} />
        </Flex>
      </Flex>
    </>
  );
};

export default HeaderLayout;
