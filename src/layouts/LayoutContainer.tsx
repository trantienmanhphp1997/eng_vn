import React, { useState } from "react";
import SidebarLayout from "./SidebarLayout.tsx";
import HeaderLayout from "./HeaderLayout.tsx";
import FooterLayout from "./FooterLayout.tsx";
import { Navigate } from "react-router-dom";
import { Breadcrumb, Button, Layout } from "antd";
import Sider from "antd/es/layout/Sider";
import { RiMenuFold3Line2 } from "react-icons/ri";
import { RiMenuFold4Line } from "react-icons/ri";
import "../style.css";
import BreadcrumbLayout from "./BreadcrumbLayout.tsx";

const LayoutContainer = ({ children }) => {
  const auth = localStorage.getItem("token");
  const { Sider, Header, Footer, Content } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  return (
    <>
      <Layout>
        <Sider
          theme="light"
          trigger={null}
          collapsible
          collapsed={collapsed}
          className="sider"
        >
          <SidebarLayout />
          <Button
            type="text"
            icon={collapsed ? <RiMenuFold4Line /> : <RiMenuFold3Line2 />}
            onClick={() => setCollapsed(!collapsed)}
            className="trigger-btn"
          />
        </Sider>
        <Layout>
          <Header className="header">
            <HeaderLayout />
          </Header>
          <Breadcrumb
            className="breadcrumb"
            items={[
              {
                title: "Home",
              },
              {
                title: <a href="">User</a>,
              },
            ]}
          />
          {/* <BreadcrumbLayout /> */}
          <Content className="content">{children}</Content>
          {/* <Footer className="footer"></Footer> */}
        </Layout>
      </Layout>
    </>
  );
};

export default LayoutContainer;
