import React, { useState } from "react";
import LayoutContainer from "../../layouts/LayoutContainer.tsx";
import { Button, Form, Input, Modal, Space, Table, TableProps, Tag } from "antd";
import { IoMdAddCircleOutline } from "react-icons/io";
import TableCustom from "../../components/TableCustom.tsx";
import ModalForm from "../../components/ModalForm.tsx";

const User = () => {
  type FieldType = {
    username?: string;
    password?: string;
  };
  interface DataType {
    key: string;
    name: string;
    age: number;
    address: string;
    tags: string[];
  }
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState<any>([
    {
      key: "1",
      name: "John Brown",
      phone: "0123456789",
      address: "New York No. 1 Lake Park",
      email: "manhtt@global.com",
    },
    {
      key: "2",
      name: "John Brown",
      phone: "0123456789",
      address: "New York No. 1 Lake Park",
      email: "manhtt@global.com",
    },
    {
      key: "1",
      name: "John Brown",
      phone: "0123456789",
      address: "New York No. 1 Lake Park",
      email: "manhtt@global.com",
    },
    {
      key: "1",
      name: "John Brown",
      phone: "0123456789",
      address: "New York No. 1 Lake Park",
      email: "manhtt@global.com",
    },
  ]);
  const columns: TableProps<DataType>["columns"] = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
  ];
  const handleOk = () => {};
  const onSubmit=(values:any)=>{
    console.log(values)
  }
  return (
    <>
      <LayoutContainer>
        <div style={{ float: "right" }}>
          <Button
            type="primary"
            icon={<IoMdAddCircleOutline />}
            size="large"
            className="btn-add"
            onClick={() => setShowModal(true)}
          >
            Add
          </Button>
        </div>
        <TableCustom data={data} columns={columns} pageIndex={1} pageSize={10}/>
        <ModalForm title={"Create"} open={showModal} toggle={()=>setShowModal(!showModal)} submit={onSubmit}>
        <Form.Item<FieldType>
              label="Username"
              name="username"
              rules={[
                { required: true, message: "Please input your username!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item<FieldType>
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>
        </ModalForm>
      </LayoutContainer>
    </>
  );
};

export default User;
