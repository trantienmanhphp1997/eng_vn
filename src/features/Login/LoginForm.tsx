import React from "react";
import { Alert, Button, Form, Input } from "antd";
import "../../style.css";
import { login } from "../../services/axios.tsx";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export default function LoginForm() {
  type FieldType = {
    username?: string;
    password?: string;
    remember?: string;
  };
  const navigate = useNavigate();

  const handleLogin = (values: any) => {
    login(values.username, values.password)
      .then((res: any) => {
        localStorage.setItem("token", res.data.token);
        toast.success("Login Success");
        navigate("/user");
      })
      .catch((error: any) => {
        return toast.error("Login Failed");
      });
  };

  return (
    <>
      <div className="login-container">
        <div className="login-form">
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            style={{
              maxWidth: 600,
              alignItems: "center",
              padding: "30px 50px 30px 30px",
              marginRight: "30px",
            }}
            initialValues={{ remember: true }}
            autoComplete="off"
            onFinish={handleLogin}
          >
            <Form.Item<FieldType>
              label="Username"
              name="username"
              rules={[
                { required: true, message: "Please input your username!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item<FieldType>
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 12, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
}
