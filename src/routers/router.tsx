import React from "react";
import Loader from "../components/Loader.tsx";
const LoginForm = Loader(() => import("../features/Login/LoginForm.tsx"));
const User = Loader(() => import("../features/Users/User.tsx"));
const router = [
  {
    path: "/login",
    element: <LoginForm />,
  },
  {
    path: "/",
    element: <LoginForm />,
  },
  {
    path: "/user",
    element: <User />,
  },
  {
    path: "*",
    element: <>Not Found</>,
  },
];
export default router;
