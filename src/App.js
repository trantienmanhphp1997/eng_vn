import "./App.css";
import LoginForm from "./features/Login/LoginForm.tsx";
import User from "./features/Users/User.tsx";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import router from "./routers/router.tsx";
import { useRoutes } from "react-router-dom";

function App() {
  const contentPage = useRoutes(router);
  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      {contentPage}
    </>
  );
}

export default App;
