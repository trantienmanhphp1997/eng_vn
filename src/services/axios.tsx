import axios from "axios";

const login = (email: string, password: string) => {
  return axios.post("https://reqres.in/api/login", {
    email: email,
    password: password,
  });
};
export { login };
